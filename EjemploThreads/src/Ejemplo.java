import java.util.ArrayList;


public class Ejemplo extends Thread
{
	public int numero;
	public ArrayList<Integer> resultado;
	
	public Ejemplo(int n)
	{
		numero = n;
		resultado = new ArrayList<Integer>();
	}
	
	@Override
	public void run()
	{
		for (int i=1; i<numero; ++i) if (numero%i==0)
			resultado.add(i);
		
		System.out.println("Divisores de: " + numero + " " + resultado);
	}
}
