import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main 
{

	public static void main(String[] args) 
	{
		ArrayList<Integer> numeros = new ArrayList<Integer>(); 
		numeros.add(120000000);
		numeros.add(1000);
		numeros.add(1400000000);
		numeros.add(400);
		numeros.add(90000000);
		numeros.add(100);
		numeros.add(120000000);
		numeros.add(1000);
		numeros.add(1400000000);
		numeros.add(400);
		numeros.add(90000000);
		numeros.add(100);
		numeros.add(120000000);
		numeros.add(1000);
		numeros.add(1400000000);
		numeros.add(400);
		numeros.add(90000000);
		numeros.add(100);
		
		long init = System.currentTimeMillis();  // Instante inicial del procesamiento
        ExecutorService executor = Executors.newFixedThreadPool(4);
		
        for (Integer num: numeros) 
        {
            Ejemplo solucion = new Ejemplo(num);
            executor.execute(solucion);
        }
        executor.shutdown();	// Cierro el Executor
        while (!executor.isTerminated()) 
        {
        	// Espero a que terminen de ejecutarse todos los procesos 
        	// para pasar a las siguientes instrucciones 
        }
        
        long fin = System.currentTimeMillis();	// Instante final del procesamiento
        System.out.println("Tiempo total de procesamiento: "+(fin-init)/1000+" Segundos");
	}
}
